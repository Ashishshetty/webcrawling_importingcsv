class Todo < ApplicationRecord
  	# has_many :items, dependent: :destroy
  	# validates_presence_of :title, :created_by

  	def self.get_keywordsearch_data(params)
  		project_array = get_projects(params)
  	end

  	def self.get_projects(params)
      db_name = params[:db_name]
      db_pass = params[:db_pass]
      func_name = params[:func_name]
      project = params[:project]
      keywordsearch_array = eval(params[:keywordsearch_array]).to_a
      portal = params[:portal]
      keyword_matrix = Matrix[*keywordsearch_array]
      keyword_index = keyword_matrix.index(portal)[1]
      length = keyword_matrix.column(keyword_index).count
      keywords = keyword_matrix.column(3)[1..length-1]
      counts = keyword_matrix.index("count")[1]
      search_count = keyword_matrix.column(counts)[1]
      
      case func_name
      when "get_keywordsearch_purplle"
        # Purplle_keywordsearch.send(func_name,db_pass,db_name,portal,project,file)
        TodosWorker.perform_async(db_pass,db_name,portal,project,keywords,count)
      end
    end
  end
