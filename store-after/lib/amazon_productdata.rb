module Amazon_productdata
  require 'rubygems'
  require 'csv'
  require 'date'
  require 'mysql'
  require 'time'
  require 'json'
  require 'logger'
  require 'curb'
  require 'mail'
  require 'pry' 
  require 'spreadsheet'
  require 'roo'
  require 'vacuum'
  require 'rufus-scheduler'
  require_relative 'amz_page_curl.rb'

  def self.get_productdata_amazon(db_pass,db_name,portal,project,file)
    p db_name

    $con_amz = Mysql.new 'localhost', "root", "#{db_pass}" , "#{db_name}"

    db_output_table = "#{project}" + "_" + 'amazon' + "_" + "productdata"
    $con_amz.query("DROP TABLE IF EXISTS #{db_output_table}")

    $con_amz.query("CREATE TABLE IF NOT EXISTS \
      #{db_output_table}(amazon_id INT PRIMARY KEY AUTO_INCREMENT NOT NULL, \
      amazon_remote_url text, \
      amazon_title text, \
      amazon_rating text, \
      amazon_reviews_count text, \
      amazon_ratings_count text, \
      amazon_brand text, \
      amazon_nykaa_pid text, \
      amazon_productcode text, \
      amazon_category text, \
      amazon_fulfilled text, \
      amazon_seller text, \
      amazon_mrp FLOAT, \
      amazon_currp FLOAT, \
      amz_shade VARCHAR(255), \
      amazon_available VARCHAR(255), \
      amazon_useragent text ,\
      created_at DATETIME NOT NULL DEFAULT CURRENT_TIMESTAMP, \
      updated_at DATETIME NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP , \
      amazon_image text)")

    array = []
    # xlsx = Roo::Spreadsheet.open('./project.ods')

    productcode_sheet = file.sheet('productdata')  

    if productcode_sheet.column(3)[0] == 'amazon'
      length = productcode_sheet.column(3).length
      array = productcode_sheet.column(3)[1..length].compact
    end

    array.each_with_index do |row,index|

      if row.include? 'http'
        str1_markerstring = "dp/"
        str2_markerstring = "/ref"
        product_code = row[/#{str1_markerstring}(.*?)#{str2_markerstring}/m, 1]
      else 
        product_code = row
      end

      category_amazon = ""

      requested = Vacuum.new('IN')

      requested.configure(
        aws_access_key_id: 'AKIAI6WCLII5MH325KLQ',
        aws_secret_access_key: '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',
        associate_tag: 'himanimodi01-21'
        )


      retrycount = 0

      if product_code.nil? or product_code.include? "NA"
        price_empty = 0.0
        con.query("INSERT INTO #{db_output_table} (amazon_category,amazon_nykaa_pid, amazon_currp,amazon_mrp, amazon_productcode, amazon_available) VALUES ('#{category_amazon}','#{nykaa_pid}', '#{price_empty}', '#{price_empty}', '', '3')")
        return
      end

      if (index % 20) == 0 and index != 0
        p 'sleeping'
        sleep(10)
      end

      while retrycount < 3
        begin
          response = requested.item_lookup(
            query: {
              'ItemId' => product_code,
              'ResponseGroup' => "ItemAttributes, Offers, OfferFull, OfferListings,Images,Reviews"  
            }
            )
          break
        rescue
          retrycount = retrycount + 1
          p 'ashishshshshshshshshshshsh'
          sleep 3
        end
      end

      hashed_products = response.to_h

      currencyCode = ""
      strikePrice = ""
      lowestPrice = ""
      review_count = ""
      rating = ""
      shade = ""
      brand = ""
      fulfilled_by = ""
      item = hashed_products["ItemLookupResponse"]["Items"]["Item"]
      availability = false
      updated_time = Time.now.strftime("%F %H:%M:%S")

      if !item.nil?

        if(!currencyCode.eql?("INR") && !item.nil? && !item["Offers"].nil? && !item["Offers"]["Offer"].nil?)
          currencyCode = item["Offers"]["Offer"]["OfferListing"]["Price"]["CurrencyCode"]
          if(currencyCode.eql?("INR"))
            lowestPrice = item["Offers"]["Offer"]["OfferListing"]["Price"]['FormattedPrice'].gsub('INR ','').gsub(',','').to_f

            if !item["Offers"]["Offer"]["OfferListing"]["AmountSaved"].nil? 
              strikePrice = item["Offers"]["Offer"]["OfferListing"]["AmountSaved"]['FormattedPrice'].gsub('INR ','').gsub(',','').to_f
              strikePrice = strikePrice.to_f + lowestPrice.to_f
              strikePrice = strikePrice.to_f
            end

            if !item["Offers"]["Offer"]["OfferListing"]["SalePrice"].nil?

              salePrice = item["Offers"]["Offer"]["OfferListing"]["SalePrice"]['FormattedPrice'].gsub('INR ','').gsub(',','').to_f
              strikePrice = lowestPrice.to_f
              lowestPrice = salePrice.to_f
              strikePrice = strikePrice.to_f
            end
            availability=true
          end
        end

        if(!currencyCode.eql?("INR") && !item.nil? && !item["OfferSummary"].nil? && !item["OfferSummary"]["LowestNewPrice"].nil?)
          puts "ITEM IS #{item}"
          currencyCode = item["OfferSummary"]["LowestNewPrice"]["CurrencyCode"]
          if(currencyCode.eql?("INR"))
            strikePrice = item["OfferSummary"]["LowestNewPrice"]['FormattedPrice'].gsub('INR ','').gsub(',','').to_f
            lowestPrice = strikePrice
          end
        end

        if(!currencyCode.eql?("INR") && !item.nil? && !item["OfferSummary"].nil? && !item["OfferSummary"]["LowestRefurbishedPrice"].nil?)
          currencyCode = item["OfferSummary"]["LowestRefurbishedPrice"]["CurrencyCode"]
          if(currencyCode.eql?("INR"))
            strikePrice = item["OfferSummary"]["LowestRefurbishedPrice"]['FormattedPrice'].gsub('INR ','').gsub(',','').to_f
            lowestPrice = strikePrice
          end
          availability=true
        end

        if(strikePrice.to_f <= 0 )
          strikePrice = lowestPrice
        end

        if(lowestPrice.to_f <= 0 )
          lowestPrice = strikePrice
        end

        offer_price = lowestPrice

        title = item['ItemAttributes']['Title']

        if !item['ImageSets'].nil? and !item['ImageSets']['ImageSet'][0].nil?
          imageurl = item['ImageSets']['ImageSet'][0]['MediumImage']['URL'] || item['ImageSets']['ImageSet'][0]['LargeImage']['URL'] || item['ImageSets']['ImageSet'][0]['SmallImage']['URL']
        elsif  !item['ImageSets'].nil?
          imageurl = item['ImageSets']['ImageSet']['MediumImage']['URL']
        end

        !(item['Offers']['Offer'].nil?) ? seller_name = item['Offers']['Offer']['Merchant']['Name'] : seller_name = ""

        amazon_remote_url = item['DetailPageURL'].split('?').first

        if !item['ItemAttributes'].nil?
          brand = item['ItemAttributes']['Brand']
          shade = item['ItemAttributes']['Color']
        end

        puts "amazon product code #{product_code}"

        values = Amz_page_curl.pagecurl(product_code)

        if !values.nil?
          if values[0] == true
            availability = '1'
          else
            availability = '0'
          end

          if values[1] != nil
            review_count = values[1]
          else
            review_count = '0'
          end

          if values[2] != nil
            rating = values[2]
          else
            rating = '0'
          end

          if values[3] == true
            fulfilled_by = '1'
          else
            fulfilled_by = '0'
          end


          if shade == nil
            shade = ""
          end

          if !values[4].nil?
            if values[4] > 0
              offer_price = values[4]
            end
          end

          if !values[5].nil?
            seller_name = values[5]
          end

        end

        if strikePrice == "" and !values[6].nil?
          strikePrice = values[6]
        end

        if shade.nil?
          shade = ""
        end

        if brand.nil?
          brand = ""
        end

        amazon_useragent = values[7]

        p amazon_remote_url
        p amazon_title =  title
        p amazon_rating = rating
        p amazon_reviews_count = review_count
        p amazon_brand = brand
        p amazon_productcode = product_code
        p amazon_fulfilled = fulfilled_by
        p amazon_seller = seller_name
        p amazon_mrp = strikePrice
        p amazon_currp = offer_price
        p amz_shade = shade
        p amazon_available = availability
        p amazon_image = imageurl
        p amazon_nykaa_pid = ""
        p amazon_category = ""
        p amazon_ratings_count = amazon_reviews_count

        if amazon_currp == ""
          amazon_currp = 0
        end

        if amazon_mrp == ""
          amazon_mrp = 0
        end

        amazon_title = amazon_title.force_encoding("BINARY").gsub(0xA0.chr,"")
        amazon_title = amazon_title.delete 160.chr+194.chr
        amazon_title = amazon_title.force_encoding("ISO-8859-1").encode("UTF-8")
        p amazon_seller = amazon_seller.gsub(/[^0-9A-Za-z ]/,'') if !amazon_seller.nil? 
        p amazon_brand = amazon_brand.gsub(/[^0-9A-Za-z ]/,'') if !amazon_brand.nil?

        $con_amz.query("INSERT INTO #{db_output_table} (amazon_useragent, amazon_ratings_count,amazon_rating, amazon_reviews_count, amazon_fulfilled, amazon_title, amazon_brand, amazon_remote_url, amazon_image, amazon_seller, amazon_mrp ,amazon_productcode, amazon_available, amazon_currp, amz_shade, amazon_nykaa_pid, amazon_category) VALUES ('#{amazon_useragent}','#{amazon_ratings_count}', '#{amazon_rating}', '#{amazon_reviews_count}', '#{amazon_fulfilled}', '#{Mysql.escape_string(amazon_title)}', '#{Mysql.escape_string(amazon_brand)}', '#{amazon_remote_url}', '#{amazon_image}', '#{amazon_seller}', '#{amazon_mrp}', '#{amazon_productcode}', '#{amazon_available}', '#{amazon_currp}', '#{Mysql.escape_string(amz_shade)}', '#{amazon_nykaa_pid}', '#{amazon_category}')")

        amazon = Hash.new()
        amazon['amazon'] = Hash.new()
        amazon['amazon']['offer_price'] = offer_price
        amazon['amazon']['available'] = availability
        p 'AMAZOOOOONNN'
        amazon
      else
        $con_amz.query("INSERT INTO #{db_output_table} (amazon_rating, amazon_reviews_count, amazon_fulfilled, amazon_title, amazon_brand, amazon_remote_url, amazon_image, amazon_seller, amazon_mrp ,amazon_productcode, amazon_available, amazon_currp, amz_shade, amazon_nykaa_pid, amazon_category) VALUES ('', '', '', '', '', '', '', '', 0, '#{amazon_productcode}', '2', 0, '', '#{amazon_nykaa_pid}', '#{amazon_category}')")
      end
    end
    $con_amz.close if $con_amz
  end
end