module Purplle_keywordsearch
	require 'csv'
	require 'date'
	require 'mysql'
	require 'time'
	require 'json'
	require 'logger'
	require 'curb'
	require 'nokogiri'
	require 'open-uri'
	require 'pry'
	require_relative 'purplle_keyword_productdata.rb'

	$con1 = Mysql.new 'localhost', "root", "comparenow", "nykaa_ashish"

	def self.get_purplle_url(url, count, page_count, db_output_table)
		res = $con1.query("select count(*) from useragents")
		res.each_hash do |total|
			@count = total['count(*)'].to_i
		end

		retrycount = 0
		while retrycount < 3
			begin
				id = rand(@count)
				useragentresult = $con1.query("select useragent from useragents where id = #{id}")
				useragentresult.each_hash do |res|
					@user_agent = res['useragent']
				end

				if url.include? 'visitorppl'
					str = `http --follow --all --max-redirects=4 "#{url}" > purplle.html`
					sleep(2)
				else
					`http --follow --all --max-redirects=3 "#{url}" > purplle.html`
					sleep(2)
				end
				sleep(2)
				break
			rescue
				retrycount = retrycount + 1
				sleep 10
			end
		end

		keyword = url.split('q=').last

		doc = Nokogiri::HTML(open("purplle_keyword.html"))
		doc.css('.item-div').each_with_index do |prod,index|
			if !prod.css('a').empty?
				base_url = prod.css('a')[0]['href']
				p prod_url = 'https://www.purplle.com'+ base_url
				$con.query("INSERT INTO #{db_output_table} (purplle_url, purplle_keyword) VALUES ('#{prod_url}', '#{Mysql.escape_string(keyword)}')")
			end
		end
	end

	def self.get_keywordsearch_purplle(db_pass,db_name,portal,project,file)
		$con = Mysql.new 'localhost', "root", "#{db_pass}", "#{db_name}"
		db_output_table = project + "_" + 'purplle' + "_" + "keywordsearchdata" + "productcode"
		$con.query("DROP TABLE IF EXISTS #{db_output_table}")

		$con.query("CREATE TABLE IF NOT EXISTS \
			#{db_output_table} (purplle_id INT PRIMARY KEY AUTO_INCREMENT NOT NULL, \
			purplle_url text, \
			purplle_keyword text)")

		array = []
		counts = []

		# xlsx = Roo::Spreadsheet.open('./project.ods')
		productcode_sheet = file.sheet('keywordsearchdata')  

		# productcode_sheet = xlsx.sheet('keywordsearchdata')  
		if productcode_sheet.column(4)[0] == 'purplle'
			length = productcode_sheet.row(2).length
			array = productcode_sheet.column(4)[1..length].compact
		end

		productcode_sheet.row(1).each_with_index do |name,index|
			if name == 'count'
				length = productcode_sheet.row(index).length
				counts = productcode_sheet.column(index+1)[1..length]
			end
		end

		array.each_with_index do |url, index|
			count = counts[index]
			page_count = 0
			get_purplle_url(url, count, page_count, db_output_table)
		end
		get_productdata(db_pass,db_name,portal,project,db_output_table)
	end
end