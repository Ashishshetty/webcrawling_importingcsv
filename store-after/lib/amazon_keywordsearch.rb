module Amazon_keywordsearch
	extend ActiveSupport::Concern
	require 'pry'
	require 'nokogiri'
	require 'mysql'
	require_relative 'amazon_keyword_productdata.rb'

	def self.get_search_data(url,count, page_count, db_output_table)
		if count.nil?
			count = 0
		end
		retrycount = 0
		while retrycount < 3
			begin
				`http --follow "#{url}" > amazon_keyword.html`
				sleep(2)
				break
			rescue
				retrycount = retrycount + 1
				sleep 1	
			end
		end

		doc = Nokogiri::HTML(open("amazon_keyword.html"))

		if !doc.css('.s-result-item').empty?
			doc.css('.s-result-item').each_with_index do |prod, index|
				if index < count or count == 0
					if count != 0 and count < 25
						p productcode = prod.attributes['data-asin'].value
						uri = URI.parse(url)
						p base_url = prod.css('.a-link-normal')[0]['href']
						region = uri.host
						p produrl = "https://" + uri.host + "/Brainwavz-Earbuds-Isolating-Earphones-Headset/dp/" + productcode
						$con.query("insert into #{db_output_table} (productcode, produrl, region, keywordsearchurl) values('#{productcode}', '#{produrl}', '#{region}', '#{url}')")
					end

					if count == 0 or count > 25
						keywordsearchurl = url.split('&page=').first
						last_row = $con.query("SELECT id FROM #{db_output_table} where keywordsearchurl like '#{keywordsearchurl}' ORDER BY id DESC LIMIT 1")
						last_row.each_hash do |row_id|
							p @row_count = row_id['id'].to_i
							@row_count = @row_count.nil? ? 0 : @row_count
						end
						if @row_count.nil?
							@row_count = 0
						end
						if @row_count < count
							p productcode = prod.attributes['data-asin'].value
							uri = URI.parse(url)
							region = uri.host
							p base_url = prod.css('.a-link-normal')[0]['href']
							p produrl = uri.host + base_url
							p produrl = "https://" + uri.host + "/Brainwavz-Earbuds-Isolating-Earphones-Headset/dp/" + productcode
							$con.query("insert into #{db_output_table} (productcode, produrl, region, keywordsearchurl) values('#{productcode}', '#{produrl}', '#{region}', '#{url}')")
						end
					end
				end
			end

			if count == 0 or count > 50
				page_count = page_count + 1
				if page_count < 4
					if url.include? '&page'
						url = url.split('&page=').first
						url = url + "&page=#{page_count}"
						get_search_data(url,count, page_count, db_output_table)
					end
				end
			end
		end
	end

	def self.get_keywordsearch_amazon(db_pass,db_name,portal,project,file)
		$con = Mysql.new 'localhost', "root", "#{db_pass}" , "#{db_name}"

		db_output_table = project + "_" + 'amazon' + "_" + "keywordsearchdata" + "productcode"

		$con.query("DROP TABLE IF EXISTS #{db_output_table}")

		$con.query("CREATE TABLE IF NOT EXISTS \
			#{db_output_table}(id INT PRIMARY KEY AUTO_INCREMENT NOT NULL, \
			productcode varchar(30), \
			region text, \
			produrl text, \
			keywordsearchurl text, \
			created_at DATETIME NOT NULL DEFAULT CURRENT_TIMESTAMP, \
			updated_at DATETIME NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP )")

		array = []
		counts = []

		# xlsx = Roo::Spreadsheet.open('./project.ods')

		productcode_sheet = file.sheet('keywordsearchdata')  

		if productcode_sheet.column(2)[0] == 'amazon'
			length = productcode_sheet.row(2).length
			array = productcode_sheet.column(2)[1..length].compact
		end

		productcode_sheet.row(1).each_with_index do |name,index|
			if name == 'count'
				length = productcode_sheet.row(index).length
				counts = productcode_sheet.column(index+1)[1..length]
			end
		end

		array.each_with_index do |url, index|
			count = counts[index]
			page_count = 0
			url = url + "&page=#{page_count}"
			get_search_data(url, count, page_count, db_output_table)
		end
		Amazon_keyword_productdata.get_productdata(db_pass,db_name,portal,project,db_output_table)
	end
	$con.close if $con
end

