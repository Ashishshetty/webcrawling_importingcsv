module Amazon_keyword_productdata
	require 'pry'
	require 'mysql'
	require 'json'
	require 'mysql'
	require 'mail'
	require 'csv'
	require_relative 'amz_page_curl.rb'

	def self.current_value_amazon(root, db_pass, db_name, db_input_table , db_output_table, prod)
		product_code = prod['productcode']
		produrl = prod['produrl']
		values = Amz_page_curl.pagecurl(produrl)

		shade = ''
		brand = ''

		if !values.nil?
			if values[0] == true
				availability = '1'
			else
				availability = '0'
			end

			if values[1] != nil
				review_count = values[1]
			else
				review_count = '0'
			end

			if values[2] != nil
				rating = values[2]
			else
				rating = '0'
			end

			if values[3] == true
				fulfilled_by = '1'
			else
				fulfilled_by = '0'
			end


			if shade == nil
				shade = ""
			end

			if !values[4].nil?
				if values[4] > 0
					offer_price = values[4]
				end
			end

			if !values[5].nil?
				seller_name = values[5]
			end

		end

		if !values[6].nil?
			strikePrice = values[6]
		end

		if strikePrice.nil? and !offer_price.nil?
			strikePrice = offer_price
		end


		if shade.nil?
			shade = ""
		end

		if brand.nil?
			brand = ""
		end

		if !values[3].nil?
			fulfilled_by = values[3]
		else
			fulfilled_by = ""
		end

		amazon_useragent = values[7]
		imageurl = values[8]
		p amazon_brand = values[9]
		p title = values[10]
		p amazon_remote_url = values[11]
		p amazon_title =  title
		p amazon_rating = rating
		p amazon_reviews_count = review_count
		p amazon_productcode = product_code
		p amazon_fulfilled = fulfilled_by
		p amazon_seller = seller_name
		p amazon_mrp = strikePrice
		p amazon_currp = offer_price
		p amz_shade = shade
		p amazon_available = availability
		p amazon_image = imageurl
		p amazon_nykaa_pid = prod['sku']
		p amazon_category = prod['Category']
		p amazon_ratings_count = amazon_reviews_count

		if amazon_currp == ""
			amazon_currp = 0
		end

		if amazon_mrp == ""
			amazon_mrp = 0
		end
		p "im here yayyyyyy"
		amazon_title = amazon_title.force_encoding("BINARY").gsub(0xA0.chr,"")
		amazon_title = amazon_title.delete 160.chr+194.chr
		amazon_title = amazon_title.force_encoding("ISO-8859-1").encode("UTF-8")
		amazon_title = amazon_title.gsub(/[^a-zA-z\d.(), *#]/, '')
		p amazon_seller = amazon_seller.gsub(/[^0-9A-Za-z ]/,'') if !amazon_seller.nil? 
		p amazon_brand = amazon_brand.gsub(/[^0-9A-Za-z ]/,'') if !amazon_brand.nil?

		$con.query("INSERT INTO #{db_output_table} (amazon_useragent, amazon_ratings_count,amazon_rating, amazon_reviews_count, amazon_fulfilled, amazon_title, amazon_brand, amazon_remote_url, amazon_image, amazon_seller, amazon_mrp ,amazon_productcode, amazon_available, amazon_currp, amz_shade, amazon_nykaa_pid, amazon_category) VALUES ('#{amazon_useragent}','#{amazon_ratings_count}', '#{amazon_rating}', '#{amazon_reviews_count}', '#{amazon_fulfilled}', '#{Mysql.escape_string(amazon_title)}', '#{Mysql.escape_string(amazon_brand)}', '#{amazon_remote_url}', '#{amazon_image}', '#{amazon_seller}', '#{amazon_mrp}', '#{amazon_productcode}', '#{amazon_available}', '#{amazon_currp}', '#{Mysql.escape_string(amz_shade)}', '#{amazon_nykaa_pid}', '#{amazon_category}')")
	end

	def self.get_productdata(db_pass,db_name,portal,project,db_input_table)
		# db_input_sku_column = "sku"

		$con = Mysql.new 'localhost', 'root',"#{db_pass}", "#{db_name}"

		db_output_table = project + "_" + 'amazon' + "_" + "keywordsearchdata" + "productdata"

		$con.query("DROP TABLE IF EXISTS  #{db_output_table}")

		$con.query("CREATE TABLE IF NOT EXISTS \
			#{db_output_table}(amazon_id INT PRIMARY KEY AUTO_INCREMENT NOT NULL, \
			amazon_remote_url text, \
			amazon_title text, \
			amazon_rating text, \
			amazon_reviews_count text, \
			amazon_ratings_count text, \
			amazon_brand text, \
			amazon_nykaa_pid text, \
			amazon_productcode text, \
			amazon_category text, \
			amazon_fulfilled text, \
			amazon_seller text, \
			amazon_mrp FLOAT, \
			amazon_currp FLOAT, \
			amz_shade VARCHAR(255), \
			amazon_available VARCHAR(255), \
			amazon_useragent text ,\
			created_at DATETIME NOT NULL DEFAULT CURRENT_TIMESTAMP, \
			updated_at DATETIME NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP , \
			amazon_image text)")

		res = $con.query("SELECT * FROM #{db_input_table}")
		res.each_hash do |prod|
			current_value_amazon("root", "#{db_pass}", "#{db_name}", "#{db_input_table}", "#{db_output_table}", prod)
		end
	end
end