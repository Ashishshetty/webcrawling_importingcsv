class Product < ActiveRecord::Base
  include Amazon_productdata
  include Flipkart_productdata
  include Purplle_productdata
  include Paytm_productdata

  include Amazon_keyword_productdata
  include Amazon_keywordsearch
  include Purplle_keyword_productdata
  include Purplle_keywordsearch
  include Amz_page_curl
  include Create_csv

  # def self.to_csv(options = {})
  #   CSV.generate(options) do |csv|
  #     csv << column_names
  #     all.each do |product|
  #       csv << product.attributes.values_at(*column_names)
  #     end
  #   end
  # end
  
  def self.import(file)
    spreadsheet = open_spreadsheet(file)
  end

  def self.open_spreadsheet(file)
    File.extname(file.original_filename)
    xlsx_projectlist = Roo::Spreadsheet.open(file.path, extension: :ods)
    @passed_file_project = Roo::Spreadsheet.open(file.path, extension: :ods)
    multi_threads(xlsx_projectlist)
  end

  def self.get_data(portal,project,datatype,file)
    func_name = "get_" + datatype + "_" + portal
    table_name = "#{project}" + "_" + portal + "_" + datatype + "dataproductdata"
    puts "#{func_name} #{project}"
    keywordsearch_array = file.sheet('keywordsearchdata').to_a
    headers = keywordsearch_array[0]
    keyword_array = keywordsearch_array.find { |x| x[0] == project }
    return headers, keyword_array
  end

  def self.productdata_method(db_pass,db_name,portal,project,datatype,file)
    func_name = "get_" + datatype + "_" + portal
    table_name = "#{project}" + "_" + portal + "_" + "productdata"
    puts "#{func_name} #{project}"
    case func_name
    when "get_productdata_amazon"
      Amazon_productdata.send(func_name,db_pass,db_name,portal,project,file)
      Create_csv.create_csv(db_pass,db_name,portal,table_name)
    when "get_productdata_purplle"
      Purplle_productdata.send(func_name,db_pass,db_name,portal,project,file)
      Create_csv.create_csv(db_pass,db_name,portal,table_name)
    when "get_productdata_flipkart"
      Flipkart_productdata.send(func_name,db_pass,db_name,portal,project,file)
      Create_csv.create_csv(db_pass,db_name,portal,table_name)
    when "get_productdata_paytm"
      Paytm_productdata.send(func_name,db_pass,db_name,portal,project,file)
      Create_csv.create_csv(db_pass,db_name,portal,table_name)
    end
  end

  def self.method_call_keywordsearch(db_pass,db_name,portal,project,datatype,file)
    func_name = "get_" + datatype + "_" + portal
    table_name = "#{project}" + "_" + portal + "_" + datatype + "dataproductdata"
    case func_name
    when "get_keywordsearch_amazon"
      Amazon_keywordsearch.send(func_name,db_pass,db_name,portal,project,file)
      Create_csv.create_csv(db_pass,db_name,portal,table_name)

    when "get_keywordsearch_purplle"
      keywordsearch_array = get_data(portal,project,datatype,file)
      keywordsearch_array = keywordsearch_array.to_s
      uri = URI.parse("http://localhost:3000/todos")
      request = Net::HTTP.post_form(uri, keywordsearch_array: keywordsearch_array,portal: portal, project: project, db_pass: db_pass, db_name: db_name, func_name: func_name )
      # Purplle_keywordsearch.send(func_name,db_pass,db_name,portal,project,file)
      # Create_csv.create_csv(db_pass,db_name,portal,table_name)
    end
  end

  def self.method_call(*arg)
    db_pass = 'comparenow'
    db_name  = arg[0][1]
    portal = arg[0][2]
    project = arg[0][3]
    row_no = arg[0][4]
    sleep_time = arg[0][5]
    file = arg[0][6]

    con = Mysql.new 'localhost' , "root" , "#{db_pass}" ,"olx"
    con.query("CREATE DATABASE IF NOT EXISTS #{db_name} ")
    threads_fun = []
    x = 1
    sleep(sleep_time)

    @project_list = file.sheet('projectlist')
    if @project_list.row(row_no)[1] == 1
      datatype = @project_list.row(1)[x]
      threads_fun << Thread.new { Thread.current[:output] =   productdata_method(db_pass,db_name,portal,project,datatype,file) } 
      threads_fun[threads_fun.length-1].join
      x +=1
    end

    if @project_list.row(row_no)[4] == 1
      datatype = "keywordsearch"
      threads_fun << Thread.new { Thread.current[:output] =  method_call_keywordsearch(db_pass,db_name,portal,project,datatype,file) }
      threads_fun[threads_fun.length-1].join
    end
  end

  def self.multi_threads(xlsx_projectlist)
    @project_list = xlsx_projectlist.sheet('projectlist')
    proj_array = []
    j = 2
    while j <=  @project_list.last_row 
      db_pass = "comparenow"
      db_name = @project_list.row(j)[0] + "_" +"data"
      project =  @project_list.row(j)[0]
      portal = @project_list.row(j)[5]
      file = xlsx_projectlist
      sleep_time = rand(1..5)
      proj_array << [db_pass,db_name,portal,project,j,1,file]
      @queue = proj_array.inject(Queue.new, :push)
      j +=1
    end

    num_threads = 1

    Thread.abort_on_exception = true
    @threads = Array.new(num_threads) do
      Thread.new do
        until @queue.empty?
          next_object = @queue.shift
          method_call(next_object)
        end
      end
    end

    begin
      @threads.each(&:join)
    ensure
      # cleanup()
      puts "ashish"
    end
  end
end
