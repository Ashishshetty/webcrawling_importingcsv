require 'sidekiq/web'

Store::Application.routes.draw do
  resources :products do
    collection { post :import }
  end
  root to: 'products#index'
  mount Sidekiq::Web, at: '/sidekiq'
end
