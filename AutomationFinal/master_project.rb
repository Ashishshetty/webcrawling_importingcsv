  require 'rubygems' 
  require 'json'
  require 'logger'
  require 'mysql'
  require 'pry'                  
  require 'nokogiri'
  require 'open-uri'
  require 'csv'
  require 'logger'
  require 'write_xlsx'
  require 'spreadsheet'
  require 'roo'

  require_relative 'flipkart_productdata.rb'
  require_relative 'amazon_productdata.rb'
  require_relative 'paytm_productdata.rb'
  require_relative 'purplle_productdata.rb'
  require_relative 'amazon_keywordsearch.rb'
  require_relative 'purplle_keywordsearch.rb'

  # require_relative 'sd_productdata.rb' 
  # require_relative 'sd_sellerdata.rb' 
  # require_relative 'fk_sellerdata.rb' 
  # require_relative 'function_infra.rb' 


  # xlsx_projectlist = Roo::Spreadsheet.open('project.xlsx')
  # xlsx_projectlist = Roo::Excelx.new('project.xlsx')

  # xlsx_projectcode = Roo::Spreadsheet.open('project.xlsx')
  # xlsx_projectcode = Roo::Excelx.new('project.xlsx')

  xlsx_projectlist = Roo::Spreadsheet.open('./project.ods')

  @project_list = xlsx_projectlist.sheet('projectlist')	
  
  
  def productdata_method(db_pass,db_name,portal,project,datatype)
    func_name = "get_" + datatype + "_" + portal
    # get_productdata_amazon
    puts "#{func_name} #{project}"
    send(func_name,db_pass,db_name,portal,project)
  end

  def method_call_keywordsearch(db_pass,db_name,portal,project,datatype)
    func_name = "get_" + datatype + "_" + portal
    puts "#{func_name} #{project}"
    send(func_name,db_pass,db_name,portal,project)
  end

  def method_call(*arg)
    db_pass = 'comparenow'
    db_name  = arg[0][1]
    portal = arg[0][2]
    project = arg[0][3]
    row_no = arg[0][4]
    sleep_time = arg[0][5]
    con = Mysql.new 'localhost' , "root" , "#{db_pass}" ,"olx"
    con.query("CREATE DATABASE IF NOT EXISTS #{db_name} ")
    threads_fun = []
    x = 1
    sleep(sleep_time)
    while x <  @project_list.last_column - 3
      if @project_list.row(row_no)[x] == 0
        x +=1
        next
      end

      datatype = @project_list.row(1)[x]
      threads_fun << Thread.new { Thread.current[:output] =   productdata_method(db_pass,db_name,portal,project,datatype) }	
      threads_fun[threads_fun.length-1].join
      x +=1
    end

    if @project_list.row(row_no)[4] == 1
      datatype = "keywordsearch"
      threads_fun << Thread.new { Thread.current[:output] =  method_call_keywordsearch(db_pass,db_name,portal,project,datatype) }
      threads_fun[threads_fun.length-1].join
    end
  end

  proj_array = []
  j = 2
  while j <=  @project_list.last_row  
    db_pass = "comparenow"
    db_name = @project_list.row(j)[0] + "_" +"data"
    project =  @project_list.row(j)[0]
    portal = @project_list.row(j)[5]
    sleep_time = rand(1..5)
    proj_array << [db_pass,db_name,portal,project,j,sleep_time]
    @queue = proj_array.inject(Queue.new, :push)
    j +=1
  end

  num_threads = 1

  Thread.abort_on_exception = true
  @threads = Array.new(num_threads) do
    Thread.new do
      until @queue.empty?
        next_object = @queue.shift
        method_call(next_object)
      end
    end
  end

  begin
    @threads.each(&:join)
  ensure
    cleanup()
    puts "ashish"
  end



