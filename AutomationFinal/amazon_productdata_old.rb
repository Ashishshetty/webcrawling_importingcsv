require 'vacuum'
require 'httparty'
require 'nokogiri'
require 'pry'
require 'mysql'
require 'json'
require 'rufus-scheduler'
require 'date'
require 'mysql'
require 'mail'
require 'csv'
require_relative 'amz_page_curl.rb'


def create_csv(db_user,db_pass,db_name,db_output_table, filename)

  if File.exist?("#{filename}")
      `rm -rf #{filename}`
  end

  ch = CSV.open("#{filename}","ab")

  con = Mysql.new 'localhost', db_user, db_pass, db_name
  ch << ["Portal","Product Name","Brand","Category","Product Code","Link","Product Image","Shade","Rating","No Of Ratings", "No Of Review", "MRP_Portal", "Price","Availibility","Seller","Fulfilled"]
  res= con.query("select * from #{db_output_table}");

  res.each_hash do |row|
    
    ch << ['Amazon',row['amazon_title'],row['amazon_brand'],row['amazon_category'],row['amazon_productcode'],row['amazon_remote_url'],row['amazon_image'],row['amz_shade'],row['amazon_rating'],row['amazon_ratings_count'], row['amazon_reviews_count'], row['amazon_mrp'], row['amazon_currp'], row['amazon_available'], row['amazon_seller'], row['amazon_fulfilled']]
   
  end
end

def current_value_amazon(root, db_pass, db_name, db_input_table , db_output_table, prod)

	con = Mysql.new 'localhost', 'root',"#{db_pass}", "#{db_name}"

	requested = Vacuum.new('IN')
	
	requested.configure(
		aws_access_key_id: 'AKIAIWDGQ4VEOF5XDU4A',
		aws_secret_access_key: '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',
		associate_tag: 'milansharma01-21'
		)

	p '#############################'
	# p amazon_nykaa_pid = prod['sku']
	p '#############################'

	retrycount = 0
	# product_code = prod['AZ_productcode']
	product_code = prod['productcode']
	# category_amazon = prod['Category']
	category_amazon = ""
	# nykaa_pid = prod['sku']
	nykaa_pid = ""

  	if product_code.nil? or product_code.include? "NA"
            price_empty = 0.0
            con.query("INSERT INTO #{db_output_table} (amazon_category,amazon_nykaa_pid, amazon_currp,amazon_mrp, amazon_productcode, amazon_available) VALUES ('#{category_amazon}','#{nykaa_pid}', '#{price_empty}', '#{price_empty}', '', '3')")
          return
        end

	if (prod['id'].to_i % 20) == 0
		sleep(60)
	end

	while retrycount < 3
		begin
			response = requested.item_lookup(
				query: {
					'ItemId' => product_code,
					'ResponseGroup' => "ItemAttributes, Offers, OfferFull, OfferListings,Images,Reviews"	
				}
				)
			break
		rescue
			retrycount = retrycount + 1
			sleep 3
		end
	end

	hashed_products = response.to_h

	currencyCode = ""
	strikePrice = ""
	lowestPrice = ""
	review_count = ""
	rating = ""
	shade = ""
	brand = ""
	fulfilled_by = ""
	binding.pry
	item = hashed_products["ItemLookupResponse"]["Items"]["Item"]
	availability = false
	updated_time = Time.now.strftime("%F %H:%M:%S")

	if !item.nil?

		if(!currencyCode.eql?("INR") && !item.nil? && !item["Offers"].nil? && !item["Offers"]["Offer"].nil?)
			currencyCode = item["Offers"]["Offer"]["OfferListing"]["Price"]["CurrencyCode"]
			if(currencyCode.eql?("INR"))
				lowestPrice = item["Offers"]["Offer"]["OfferListing"]["Price"]['FormattedPrice'].gsub('INR ','').gsub(',','').to_f

				if !item["Offers"]["Offer"]["OfferListing"]["AmountSaved"].nil?	
					strikePrice = item["Offers"]["Offer"]["OfferListing"]["AmountSaved"]['FormattedPrice'].gsub('INR ','').gsub(',','').to_f
					strikePrice = strikePrice.to_f + lowestPrice.to_f
					strikePrice = strikePrice.to_f
				end

				if !item["Offers"]["Offer"]["OfferListing"]["SalePrice"].nil?

					salePrice = item["Offers"]["Offer"]["OfferListing"]["SalePrice"]['FormattedPrice'].gsub('INR ','').gsub(',','').to_f
					strikePrice = lowestPrice.to_f
					lowestPrice = salePrice.to_f
					strikePrice = strikePrice.to_f
				end
				availability=true
			end
		end

		if(!currencyCode.eql?("INR") && !item.nil? && !item["OfferSummary"].nil? && !item["OfferSummary"]["LowestNewPrice"].nil?)
			puts "ITEM IS #{item}"
			currencyCode = item["OfferSummary"]["LowestNewPrice"]["CurrencyCode"]
			if(currencyCode.eql?("INR"))
				strikePrice = item["OfferSummary"]["LowestNewPrice"]['FormattedPrice'].gsub('INR ','').gsub(',','').to_f
				lowestPrice = strikePrice
			end
		end

		if(!currencyCode.eql?("INR") && !item.nil? && !item["OfferSummary"].nil? && !item["OfferSummary"]["LowestRefurbishedPrice"].nil?)
			currencyCode = item["OfferSummary"]["LowestRefurbishedPrice"]["CurrencyCode"]
			if(currencyCode.eql?("INR"))
				strikePrice = item["OfferSummary"]["LowestRefurbishedPrice"]['FormattedPrice'].gsub('INR ','').gsub(',','').to_f
				lowestPrice = strikePrice
			end
			availability=true
		end

		if(strikePrice.to_f <= 0 )
			strikePrice = lowestPrice
		end

		if(lowestPrice.to_f <= 0 )
			lowestPrice = strikePrice
		end

		offer_price = lowestPrice

		title = item['ItemAttributes']['Title']

		if !item['ImageSets'].nil? and !item['ImageSets']['ImageSet'][0].nil?
			imageurl = item['ImageSets']['ImageSet'][0]['MediumImage']['URL'] || item['ImageSets']['ImageSet'][0]['LargeImage']['URL'] || item['ImageSets']['ImageSet'][0]['SmallImage']['URL']
		elsif  !item['ImageSets'].nil?
			imageurl = item['ImageSets']['ImageSet']['MediumImage']['URL']
		end

		!(item['Offers']['Offer'].nil?) ? seller_name = item['Offers']['Offer']['Merchant']['Name'] : seller_name = ""

		amazon_remote_url = item['DetailPageURL'].split('?').first

		if !item['ItemAttributes'].nil?
			brand = item['ItemAttributes']['Brand']
			shade = item['ItemAttributes']['Color']
		end

		puts "amazon product code #{product_code}"

		
		values = pagecurl(product_code)
		
		if !values.nil?
			if values[0] == true
				availability = '1'
			else
				availability = '0'
			end

			if values[1] != nil
				review_count = values[1]
			else
				review_count = '0'
			end

			if values[2] != nil
				rating = values[2]
			else
				rating = '0'
			end

			if values[3] == true
				fulfilled_by = '1'
			else
				fulfilled_by = '0'
			end


			if shade == nil
				shade = ""
			end

			if !values[4].nil?
				if values[4] > 0
					offer_price = values[4]
				end
			end

			if !values[5].nil?
				seller_name = values[5]
			end
		
		end

		if strikePrice == "" and !values[6].nil?
			strikePrice = values[6]
		end

		if shade.nil?
			shade = ""
		end

		if brand.nil?
			brand = ""
		end

		amazon_useragent = values[7]

		p amazon_remote_url
		p amazon_title =  title
		p amazon_rating = rating
		p amazon_reviews_count = review_count
		p amazon_brand = brand
		p amazon_productcode = product_code
		p amazon_fulfilled = fulfilled_by
		p amazon_seller = seller_name
		p amazon_mrp = strikePrice
		p amazon_currp = offer_price
		p amz_shade = shade
		p amazon_available = availability
		p amazon_image = imageurl
		p amazon_nykaa_pid = prod['sku']
		p amazon_category = prod['Category']
		p amazon_ratings_count = amazon_reviews_count
			
		if amazon_currp == ""
			amazon_currp = 0
		end

		if amazon_mrp == ""
			amazon_mrp = 0
		end

		amazon_title = amazon_title.force_encoding("BINARY").gsub(0xA0.chr,"")
		amazon_title = amazon_title.delete 160.chr+194.chr
		amazon_title = amazon_title.force_encoding("ISO-8859-1").encode("UTF-8")
		p amazon_seller = amazon_seller.gsub(/[^0-9A-Za-z ]/,'') if !amazon_seller.nil? 
		p amazon_brand = amazon_brand.gsub(/[^0-9A-Za-z ]/,'') if !amazon_brand.nil?
		#binding.pry
		con.query("INSERT INTO #{db_output_table} (amazon_useragent, amazon_ratings_count,amazon_rating, amazon_reviews_count, amazon_fulfilled, amazon_title, amazon_brand, amazon_remote_url, amazon_image, amazon_seller, amazon_mrp ,amazon_productcode, amazon_available, amazon_currp, amz_shade, amazon_nykaa_pid, amazon_category) VALUES ('#{amazon_useragent}','#{amazon_ratings_count}', '#{amazon_rating}', '#{amazon_reviews_count}', '#{amazon_fulfilled}', '#{Mysql.escape_string(amazon_title)}', '#{Mysql.escape_string(amazon_brand)}', '#{amazon_remote_url}', '#{amazon_image}', '#{amazon_seller}', '#{amazon_mrp}', '#{amazon_productcode}', '#{amazon_available}', '#{amazon_currp}', '#{Mysql.escape_string(amz_shade)}', '#{amazon_nykaa_pid}', '#{amazon_category}')")

		amazon = Hash.new()
		amazon['amazon'] = Hash.new()
		amazon['amazon']['offer_price'] = offer_price
		amazon['amazon']['available'] = availability
		p 'AMAZOOOOONNN'
		amazon
	else
		con.query("INSERT INTO #{db_output_table} (amazon_rating, amazon_reviews_count, amazon_fulfilled, amazon_title, amazon_brand, amazon_remote_url, amazon_image, amazon_seller, amazon_mrp ,amazon_productcode, amazon_available, amazon_currp, amz_shade, amazon_nykaa_pid, amazon_category) VALUES ('', '', '', '', '', '', '', '', 0, '#{amazon_productcode}', '2', 0, '', '#{amazon_nykaa_pid}', '#{amazon_category}')")
	end
end

#scheduler = Rufus::Scheduler.new
#scheduler.every '1d', :first_in => :now do
#scheduler.every '1d', first_in: Time.now + 4*60*60 + 30*60 do
#scheduler.cron '00 23 * * *' do

db_name = "nykaa_ashish"
db_pass = "comparenow"
db_output_table = "amazon_tommydata"
db_input_table = "amazon_tommy"
db_input_sku_column = "sku"

con = Mysql.new 'localhost', 'root',"#{db_pass}", "#{db_name}"
con.query("DROP TABLE IF EXISTS  #{db_output_table}")
con.query("CREATE TABLE IF NOT EXISTS \
	#{db_output_table}(amazon_id INT PRIMARY KEY AUTO_INCREMENT NOT NULL, \
	amazon_remote_url text, \
	amazon_title text, \
	amazon_rating text, \
	amazon_reviews_count text, \
	amazon_ratings_count text, \
	amazon_brand text, \
	amazon_nykaa_pid text, \
	amazon_productcode text, \
	amazon_category text, \
	amazon_fulfilled text, \
	amazon_seller text, \
	amazon_mrp FLOAT, \
	amazon_currp FLOAT, \
	amz_shade VARCHAR(255), \
	amazon_available VARCHAR(255), \
	amazon_useragent text ,\
	created_at DATETIME NOT NULL DEFAULT CURRENT_TIMESTAMP, \
	updated_at DATETIME NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP , \
	amazon_image text)")

res = con.query("SELECT * FROM #{db_input_table} ")
res.each_hash do |prod|
	current_value_amazon("root", "#{db_pass}", "#{db_name}", "#{db_input_table}", "#{db_output_table}", prod)
end

time = (Time.now + 5*60*60 + 30*60).strftime("%Y-%m-%d %H:%M")
filename = "amazondata_#{time}.csv"
create_csv("root","#{db_pass}","#{db_name}","#{db_output_table}","#{filename}")

 Mail.defaults do
 	delivery_method :smtp,  {
 		:address => 'smtp.gmail.com',
 		:port => '25',
 		:user_name => 'Insights@intellolabs.com', 
 		:password => 'intellolabs@123',
 		:enable_starttls_auto => true}
 	end

 	@mail = Mail.new do
 		from 'Insights@intellolabs.com'
          to 'ashish@comparedapp.com'
         #to 'sujith@comparedapp.com, ashish@comparedapp.com'
         # cc 'nishantm@comparedapp.com, roli@comparedapp.com'

         subject 'Nykaa Amazon Data Done'
         body 'Hi all 
         Done with the products '

     end
#  # x=Date.today.to_s
 @mail.add_file("#{filename}") 
 @mail.deliver!


#end
# scheduler.join
