require 'rubygems'
require 'csv'
require 'date'
require 'mysql'
require 'time'
require 'json'
require 'logger'
require 'curb'
require 'mail'
require 'pry' 
require 'spreadsheet'
require 'roo'

def get_productdata_flipkart(db_pass,db_name,portal,project)
  p db_name
  $con = Mysql.new 'localhost', "root", "#{db_pass}" , "#{db_name}"

  array = []
  xlsx = Roo::Spreadsheet.open('./project.ods')

  productcode_sheet = xlsx.sheet('productdata')  

  if productcode_sheet.column(2)[0] == 'flipkart'
    length = productcode_sheet.column(2).length
    array = productcode_sheet.column(2)[1..length].compact
  end

  db_output_table = 'nykaa' + "_" + 'flipkart' + "_" + "productdata"

  $con.query("DROP TABLE IF EXISTS #{db_output_table}")
  $con.query("CREATE TABLE IF NOT EXISTS \
    #{db_output_table} (flipkart_id INT PRIMARY KEY AUTO_INCREMENT NOT NULL, \
    flipkart_url text, \
    flipkart_title VARCHAR(2048), \
    flipkart_imgurl text, \
    flipkart_rating VARCHAR(500), \
    flipkart_productid VARCHAR(2048), \
    flipkart_availability VARCHAR(500), \
    flipkart_sellingprice FLOAT, \
    flipkart_mrp FLOAT, \
    flipkart_seller VARCHAR(2048), \
    flipkart_fulfilled VARCHAR(1024))")

  array.each do |row|
    # row = 'https://www.flipkart.com/redmi-note-4-dark-grey-64-gb/p/itmeqg88cnhyyuaf?pid=MOBEQ98TWG8X4HH3&srno=s_1_8&otracker=search&lid=LSTMOBEQ98TWG8X4HH30D3CZW&qH=9b6bf0057c19bd94'

    if row.include? 'http'
      str1_markerstring = "pid="
      str2_markerstring = "&"
      productcode = row[/#{str1_markerstring}(.*?)#{str2_markerstring}/m, 1]
    else 
      productcode = row
    end

    retry_count = 0
    while retry_count < 3
      begin 
        url = "https://www.flipkart.com/tommy-hilfiger-boys-printed-casual-dark-blue-shirt/p/itmetgruzs3zg6gf?pid=#{productcode}"
        `curl -L #{url} > flipkart_productdata.html`
        break
      rescue 
        retry_count = retry_count + 1
      end
    end

    availability = '1'
    fulfilled = '1'
    doc = Nokogiri::HTML(open("flipkart_productdata.html"))
    title = doc.css('h1').text
    currp = !doc.css('._37U4_g').empty? ? doc.css('._37U4_g').text.gsub!(/[^0-9]/, '').to_i : 0
    mrp = !doc.css('._16fZeb').empty? ? doc.css('._16fZeb').text.gsub!(/[^0-9]/, '').to_i : 0
    image_url = !doc.css('.sfescn').empty? ? doc.css('.sfescn')[0]['src'] : ''
    rating = !doc.css('.hGSR34').empty? ? doc.css('.hGSR34').text.delete('★') : ''
    seller = doc.css('#sellerName').text
    availability = !doc.css('.RIBRtX').empty? ? '0' : '1'
    fulfilled = !doc.css('._1wAAli').empty? ? '1' : '0'

    puts updated_time = Time.now.strftime("%F %H:%M:%S")
    $con.query("INSERT INTO #{db_output_table} (flipkart_url,flipkart_title,flipkart_imgurl,flipkart_rating,flipkart_productid,flipkart_availability,flipkart_sellingprice,flipkart_mrp,flipkart_seller,flipkart_fulfilled) VALUES ('#{url}','#{Mysql.escape_string(title)}','#{Mysql.escape_string(image_url)}','#{rating}','#{productcode}','#{Mysql.escape_string(availability)}',#{currp},#{mrp},'#{Mysql.escape_string(seller)}','#{fulfilled}')")
  end
end
