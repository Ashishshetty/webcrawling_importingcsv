require 'rufus-scheduler'
require 'rubygems'
require 'csv'
require 'date'
require 'mysql'
require 'time'
require 'json'
require 'logger'
require 'curb'
require 'pry'
require 'mail'
require 'time'
require 'open-uri'
require 'nokogiri'



def page_curl(url,db_output_table)

  `curl -L "#{url}" > paytm.html`

  doc = Nokogiri::HTML(open("paytm.html"))
  p mrp = !doc.css('[itemprop="price"]').empty? ? doc.css('[itemprop="price"]').text.strip.gsub!(/[^0-9]/, '').to_i : '0'
  p selling_price = !doc.css('._2CkE._30zs').empty? ? doc.css('._2CkE._30zs').text.strip.gsub!(/[^0-9]/, '').to_i : '0'
  if !doc.css('._12d4').empty?
    if doc.css('._12d4').text.downcase.include? "out of"
      p available = '0' 
    else 
      p available = '1'
    end
  end

  if doc.css('._12d4').empty?
   puts 'inside no div condition'
   p available = '1'
 end

 p title = !doc.css('.NZJI').empty? ? doc.css('.NZJI').text.strip : ""
 p brand = !doc.css('.K_rF a').text.nil? ? doc.css('.K_rF a').text : ""
 p remote_url = url
 p image_url = !doc.css('._1-Zc img')[0].nil? ? doc.css('._1-Zc img')[0]['src'] : ""
 p sellername = !doc.css('._1REL').empty? ? doc.css('._1REL').text : ''

 doc.css('.w3LC').each_with_index do |div,index|
  if div.text.downcase.include? 'product code'
    @productcode = !doc.css('.FqsW').css('._2LOI')[index].nil? ? doc.css('.FqsW').css('._2LOI')[index].text : ""
    break
  end
end
$con.query("INSERT INTO #{db_output_table} (title,brand,remote_url,image_url,seller,mrp,productcode,available,currp) VALUES ('#{Mysql.escape_string(title)}','#{Mysql.escape_string(brand)}','#{url}','#{image_url}','#{Mysql.escape_string(sellername)}','#{mrp}','#{@productcode}','#{available}','#{selling_price}')")
end

def get_productdata_paytm(db_pass,db_name,portal,project)
  p db_name
  $con = Mysql.new 'localhost', "root", "#{db_pass}" , "#{db_name}"

  array = []
  xlsx = Roo::Spreadsheet.open('./project.ods')

  productcode_sheet = xlsx.sheet('productdata')  

  if productcode_sheet.column(5)[0] == 'paytm'
    length = productcode_sheet.column(5).length
    array = productcode_sheet.column(5)[1..length].compact
  end

  db_output_table = project + "_" + 'paytm' + "_" + "productdata"

  $con.query("DROP TABLE IF EXISTS #{db_output_table}")

  $con.query("CREATE TABLE IF NOT EXISTS \
    #{db_output_table}(id INT PRIMARY KEY AUTO_INCREMENT NOT NULL, \
    title VARCHAR(2048), \
    brand VARCHAR(1024), \
    remote_url text, \
    image_url text, \
    seller VARCHAR(1024), \
    mrp VARCHAR(1024), \
    productcode varchar(1024), \
    available TINYINT(1), \
    currp VARCHAR(1024),\
    created_at DATETIME NOT NULL DEFAULT CURRENT_TIMESTAMP, \
    updated_at DATETIME NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP )")

  array.each do |row|
    url = row
    p url
    # url = "https://paytm.com/shop/p/lg-l-nova-plus-lsa3np3a-1-ton-3-star-split-ac-LARLG-L-NOVA-PLGSR-406134A83109E"
    page_curl(url,db_output_table)
  end
  $con.close if $con
end


