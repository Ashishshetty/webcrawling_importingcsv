require 'rufus-scheduler'
require 'date'
require 'time'
require 'rubygems'
require 'nokogiri'
require 'pry'
require 'mysql'
require 'open-uri'
require 'json'
require 'csv'
require 'logger'
require 'mail'
require 'test-unit'

def create_input(db_pass,db_name,project)
	puts " ---#{db_pass}----#{db_name}------#{project} "
	sleep(2)
 	con = Mysql.new 'localhost', "root", db_pass , db_name
	input_table = project + "_" + "input"
	con.query("DROP TABLE IF EXISTS #{input_table}")
	
	con.query("create table IF NOT EXISTS #{input_table} \
		(k_id INT PRIMARY KEY AUTO_INCREMENT NOT NULL, \
		kent_brand varchar(100), \
		kent_portal varchar(100), \
		kent_productcode varchar(30), \
		kent_mrp FLOAT, \
		kent_mop FLOAT, \
		kent_name varchar(2000), \
		kent_category varchar(500) )");
	puts "------------------------#{project}------------------"
	CSV.foreach("input.csv") do |row|
		puts kent_brand=row[1]
		puts kent_productcode = row[6]
		puts kent_portal = row[9]
		puts kent_name = row[12]
		puts kent_mrp = row[13]
		puts kent_mop = row[14]
		puts kent_category = row[15]
		puts "==================="

		con.query("INSERT INTO #{input_table} (kent_brand,kent_portal,kent_productcode,kent_mrp,kent_mop,kent_name,kent_category) VALUES ('#{kent_brand}','#{kent_portal}','#{kent_productcode}','#{row[13]}','#{row[14]}','#{row[12]}','#{row[15]}');")
	end
end

def merge_productdata(db_pass,db_name,portal,project)
	producttable =  project + "_" + portal + "_" + "productdata"
	mergeinitial =  project + "_" + portal + "_" + "merge_productdata"
	input_table = project + "_" + "input"
	con = Mysql.new 'localhost', "root", "#{db_pass}", "#{db_name}"
	con.query("DROP TABLE IF EXISTS #{mergeinitial}")
	con.query("create table #{mergeinitial} as (SELECT * from #{input_table} LEFT JOIN #{producttable} ON  #{input_table}.kent_productcode=#{producttable}.productcode)")
end

def merge_productdata_sellerdata(db_pass,db_name,portal,project)

	producttable =  project + "_" + portal + "_" + "productdata"
	sellertable =  project + "_" + portal + "_" + "sellerdata"
	mergeinitial =  project + "_" + portal + "_" + "merge_productdata"
	mergefinal =  project + "_" + portal + "_" + "merge_productdata_seller"
	input_table = project + "_" + "input"

	con = Mysql.new 'localhost', "root", "#{db_pass}", "#{db_name}"


	con.query("DROP TABLE IF EXISTS #{mergefinal}")

	con.query("create table #{mergefinal} as (SELECT * from #{mergeinitial} LEFT JOIN #{sellertable} ON #{mergeinitial}.kent_productcode=#{sellertable}.s_productcode)")

	con.query("alter table #{mergefinal} add violation_price varchar(50)")

	con.query("alter table #{mergefinal} add violation_mrp varchar(50)")

	con.query("alter table #{mergefinal} add discount varchar(50)")

	

	res= con.query("select * from #{mergefinal}");

	res.each_hash do |row|

		id = row['s_id']

		kent_mrp = row['kent_mrp']
		puts "kent_mrp = "+kent_mrp

		kent_mop = row['kent_mop']
		puts "kent_mop = "+kent_mop

		s_mrp = row['s_mrp'].gsub("Rs.","").gsub(",","").gsub("/-","")
		puts "mrp = "+s_mrp

		s_currprice = row['s_currp'].gsub("Rs.","").gsub(",","").gsub("/-","")
		puts "currprice = "+s_currprice

		discount = ((kent_mrp.to_i-s_currprice.to_i)*100)/kent_mrp.to_i
		

		if kent_mrp==s_mrp
			mrp_violation="no"
			puts "mrp_violation = "+mrp_violation
		else
			mrp_violation="yes"
			puts "mrp_violation = "+mrp_violation
		end			

		if s_currprice<kent_mop
			currp_violaiton="yes"
			puts "currp_violaiton = "+currp_violaiton
		else
			currp_violaiton="no"
			puts "currp_violaiton = "+currp_violaiton

		end


		con.query("update #{mergefinal} set violation_price='#{currp_violaiton}',violation_mrp='#{mrp_violation}',discount='#{discount}' where s_id=#{id}")

		puts "===============#{id}=================="
		
	end
end





