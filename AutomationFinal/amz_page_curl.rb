require 'csv'
require 'date'
require 'mysql'
require 'time'
require 'json'
require 'logger'
require 'curb'
require 'nokogiri'
require 'open-uri'
require 'pry'

def pagecurl(productcode)

	url = "http://www.amazon.in/dp/#{productcode}/ref=olp_product_details?"

	retrycount = 0

	con1 = Mysql.new 'localhost', 'root',"comparenow", "nykaa_ashish"

	res = con1.query("select count(*) from useragents")
	res.each_hash do |total|
		@count = total['count(*)'].to_i
	end

	while retrycount < 3
		begin
			id = rand(@count)
			useragentresult = con1.query("select useragent from useragents where id = #{id}")
			useragentresult.each_hash do |res|
				@user_agent = res['useragent']
			end
			p @user_agent
			`curl -A "#{@user_agent}" "#{url}" > mobilescource.html`
			sleep(5)
			break
		rescue
			con1.query("update useragents set status = 0 where useragent = '#{@user_agent}' ")
			retrycount = retrycount + 1
			sleep 3
		end
	end

	doc = Nokogiri::HTML(open("mobilescource.html"))

	if !doc.title.nil?
		until !doc.title.nil? and !doc.title.downcase.include? 'robot'
			id = rand(@count)
			useragentresult = con1.query("select useragent from useragents where id = #{id}")
			useragentresult.each_hash do |res|
				@user_agent = res['useragent']
			end
			p @user_agent
			`curl -A "#{@user_agent}" "#{url}" > mobilescource.html`
			doc = Nokogiri::HTML(open("mobilescource.html"))
			sleep 5
		end
	end


	if !doc.css('#availability').nil?
		availability = doc.css('#availability').text.strip.downcase.include? 'unavailable'
	end

	if !doc.css('#acrCustomerReviewText').nil?
		review_count = doc.css('#acrCustomerReviewText').text.split(' ')[0]
	end

	if review_count.nil? and !doc.css('.a-size-mini').empty?
		review_count = doc.css('.a-size-mini').text
	end

	if !review_count.nil? and review_count.length > 20
		review_count = "0"
	end

	if review_count.nil? and !doc.css('.totalReviewCount').empty?
		review_count = doc.css('.totalReviewCount').text.split(' ')[0]
	end

	if !doc.css('#acrPopover')[0].nil?
		begin
			rating = doc.css('#acrPopover')[0].text.strip.split(' ')[0]
		rescue
			rating = "0"
		end
	end

	if !doc.css('#priceblock_ourprice').empty?
		curl_price = doc.css('#priceblock_ourprice').text
		if curl_price != ""
			curl_price = curl_price.split(' ')[1].gsub(',','').to_f
		else
			curl_price = curl_price.t_f
		end
	end

	if !doc.css('#priceblock_dealprice').empty?
		deal_price = doc.css('#priceblock_dealprice').text 
		if doc.css('#priceblock_dealprice').text != ""
			deal_price = deal_price.split(' ')[1].gsub(',','').to_f 
		else
			deal_price = deal_price.to_f
		end
	end

	if !deal_price.nil? and deal_price > 0
		curl_price = deal_price
	end

	if !doc.css('#merchant-info a')[0].nil?
		curl_seller_name = doc.css('#merchant-info a')[0].text
	end

	if !doc.css('.dpEssentialInfoTopSection').empty?
		if !doc.css('.dpEssentialInfoTopSection .dpListPrice').empty?
			curl_strikeprice = doc.css('.dpEssentialInfoTopSection .dpListPrice').text.split(' ')[1].gsub(/[^\d.]/, '').to_f
		end
	end

	if !doc.css('.dpEssentialInfoTopSection').empty?
		if !doc.css('.dpEssentialInfoTopSection .dpOurPrice').empty?
			curl_price = doc.css('.dpEssentialInfoTopSection .dpOurPrice').text.split(' ')[1].gsub(/[^\d.]/, '').to_f
		end
	end

	if !doc.css('.dpEssentialInfoBottomSection a').empty?
		curl_seller_name = doc.css('.dpEssentialInfoBottomSection a').text
	end

	if !doc.css('.dpRatingStars img').empty?
		rating = doc.css('.dpRatingStars img')[0]['src'].split('/').last.split('._').first.delete('stars0-')
	end

	if !doc.css('.numReviews')[0].nil?
		review_count = doc.css('.numReviews')[0].text.gsub(/[^\d.]/, '')
	end

	if !doc.css('.numReviews')[0].nil?
		review_count = doc.css('.numReviews')[0].text.gsub(/[^\d.]/, '')
	end

	if !doc.css('.dpEssentialInfoBottomSection').empty?
		fulfilled_by = doc.css('.dpEssentialInfoBottomSection').text.downcase.include? 'fulfilled by amazon'
	end

	if !doc.css('.a-text-strike').empty?
		if doc.css('.a-text-strike').text.split(' ').count == 1
			curl_strikeprice = doc.css('.a-text-strike').text.gsub(/[^\d.]/, '').to_f
		elsif doc.css('.a-text-strike').text.split(' ').count == 3
			curl_strikeprice = doc.css('.a-text-strike').text.split(' ')[0].gsub(/[^\d.]/, '').to_f
		else
			curl_strikeprice = doc.css('.a-text-strike').text.split(' ')[1].gsub(',','').to_f
		end
	end

	if !doc.css('.buyingPrice').empty?
		curl_price = doc.css('span .buyingPrice')[0].text.gsub(/[^\d.]/, '').to_f
	end

	if !doc.css('.a-icon-star-mini').empty?
		rating = doc.css('.a-icon-star-mini').text.split(' ')[0]
	end

	fulfilled_by = doc.css('#ourprice_fbabadge span')[0].values[0].downcase.include? 'swsprite' if !doc.css('#ourprice_fbabadge span')[0].nil?

	if fulfilled_by == nil and !doc.css('#merchant-info').nil?
		fulfilled_by = doc.css('#merchant-info').text.strip.downcase.include? 'fulfilled by amazon'
	end

	if !doc.css('#priceblock_saleprice').empty?
		sale_price = doc.css('#priceblock_saleprice').text.split(' ')[1].gsub(',','').to_f
	end

	if !sale_price.nil? and sale_price > 0
		curl_price = sale_price
	end

	if curl_strikeprice.nil?
		curl_strikeprice = curl_price
	end

	title = doc.css('#productTitle').text.strip

	image = doc.css('#landingImage')[0]['src']

	if image.length > 1000
		image = doc.css('#landingImage')[0].values[2]
	end

	brand = doc.css('#brand').text.strip

	return !availability, review_count, rating, fulfilled_by, curl_price, curl_seller_name, curl_strikeprice, @user_agent, image, brand, title, url
end
