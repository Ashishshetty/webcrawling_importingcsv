require 'pry'
require 'watir'
require 'headless'
require 'nokogiri'
require 'mysql'
require_relative 'amazon_keyword_productdata.rb'

def get_search_data(url,count, page_count, db_output_table)
	if count.nil?
		count = 0
	end
	retrycount = 0
	while retrycount < 3
		begin
			`http --follow "#{url}" > amazon_keyword.html`
			sleep(2)
			break
		rescue
			retrycount = retrycount + 1
			sleep 1
		end
	end

	doc = Nokogiri::HTML(open("amazon_keyword.html"))

	if !doc.css('.s-result-item').empty?
		doc.css('.s-result-item').each_with_index do |prod, index|
			if index < count or count == 0	
				if count != 0
					p productcode = prod.attributes['data-asin'].value
					$con.query("insert into #{db_output_table} (productcode) values('#{productcode}')")
				end

				if count == 0
					p productcode = prod.attributes['data-asin'].value
					$con.query("insert into #{db_output_table} (productcode) values('#{productcode}')")
				end
			end
		end

		if count == 0
			page_count = page_count + 1
			if url.include? '&page'
				url = url.split('&page=').first
				url = url + "&page=#{page_count}"
				get_search_data(url,count, page_count, db_output_table)
			end
		end
	end
	$con.close if $con
end

def get_keywordsearch_amazon(db_pass,db_name,portal,project)
	$con = Mysql.new 'localhost', "root", "#{db_pass}" , "#{db_name}"

	db_output_table = project + "_" + 'amazon' + "_" + "keywordsearchdata" + "productcode"

	$con.query("DROP TABLE IF EXISTS #{db_output_table}")

	$con.query("CREATE TABLE IF NOT EXISTS \
		#{db_output_table}(id INT PRIMARY KEY AUTO_INCREMENT NOT NULL, \
		productcode varchar(30), \
		created_at DATETIME NOT NULL DEFAULT CURRENT_TIMESTAMP, \
		updated_at DATETIME NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP )")

	array = []
	counts = []

	xlsx = Roo::Spreadsheet.open('./project.ods')

	productcode_sheet = xlsx.sheet('keywordsearchdata')  

	if productcode_sheet.column(2)[0] == 'amazon'
		length = productcode_sheet.row(2).length
		array = productcode_sheet.column(2)[1..length].compact
	end

	productcode_sheet.row(1).each_with_index do |name,index|
		if name == 'count'
			length = productcode_sheet.row(index).length
			counts = productcode_sheet.column(index+1)[1..length]
		end
	end

	array.each_with_index do |url, index|
		count = counts[index]
		page_count = 0
		url = url + "&page=#{page_count}"
		get_search_data(url, count, page_count, db_output_table)
	end
	get_productdata(db_pass,db_name,portal,project,db_output_table)
end
