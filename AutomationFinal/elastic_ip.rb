require 'aws-sdk'

ec2 = Aws::EC2::Client.new(region: 'us-west-2')

instance_id = "eipalloc-ea2fcad7" # For example, "i-0a123456b7c8defg9"

def display_addresses(ec2, instance_id)
  describe_addresses_result = ec2.describe_addresses({
    filters: [
      {
        name: "instance-id",
        values: [ instance_id ]
      },
    ]
  })
  if describe_addresses_result.addresses.count == 0
    puts "No addresses currently associated with the instance."
  else
    describe_addresses_result.addresses.each do |address|
      puts "=" * 10
      puts "Allocation ID: #{address.allocation_id}"
      puts "Association ID: #{address.association_id}"
      puts "Instance ID: #{address.instance_id}"
      puts "Public IP: #{address.public_ip}"
      puts "Private IP Address: #{address.private_ip_address}"
    end
  end
end

puts "Before allocating the address for the instance...."
display_addresses(ec2, instance_id)

puts "\nAllocating the address for the instance..."
allocate_address_result = ec2.allocate_address({
  domain: "vpc" 
})

puts "\nAfter allocating the address for instance, but before associating the address with the instance..."
display_addresses(ec2, instance_id)

puts "\nAssociating the address with the instance..."
associate_address_result = ec2.associate_address({
  allocation_id: allocate_address_result.allocation_id, 
  instance_id: instance_id, 
})

puts "\nAfter associating the address with the instance, but before releasing the address from the instance..."
display_addresses(ec2, instance_id)

puts "\nReleasing the address from the instance..."
ec2.release_address({
  allocation_id: allocate_address_result.allocation_id, 
})

puts "\nAfter releasing the address from the instance..."
display_addresses(ec2, instance_id)